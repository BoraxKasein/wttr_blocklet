# wttr
Blocklet for i3blocks. Uses [wttr.in](http://wttr.in) to query the weather. Supports multiple locations.
Font with Emoji support needed for weather icons.
![](wttr1.png)
![](wttr2.png)

## Controls
Button|Action
------|------
Left mouse button|Display next location
Mouse wheel down|Display next location
Right mouse button|Display previous location
Mouse wheel up|Display previous location
Middle mouse button|Update weather


## Environment variables:
Variable|Description
--------|-----------
UPDATE\_INTERVAL | Update interval in seconds. Default: 3600
UPDATE\_MSG\_TXT | Message printed by an update. Empty string for no update message. Default: ''
LOCATION | A list of locations in format: ['loc1', 'loc2', ...]". Empty string for a location auto detects your location. Default: ['']
LOCATION\_NAME | A list of names for LOCATION. Ignored if empty list. This is useful if you want coordinates as location and give them a name. Default: []
ONLY\_TEMP | If 1 only the temperature is displayed, if 0 temperature and wind speed is displayed. Default: 0
START\_DELAY | A delay before the script starts. Is useful if you have a WiFi connection which needs some time to start. Default: 0


## Examples
Minimal config. The location is determined by wttr.in
```
[wttr]
command=$SCRIPT_DIR/wttr 
interval=persist
```


Medium config. You define the locations. They must be defined as a Python list of strings 
```
[wttr]
command=$SCRIPT_DIR/wttr 
interval=persist
LOCATION=['Berlin', 'Singapore', 'New York']

```


All possible variables. You define the location and the location name. 
LOCATION is queried from wttr.in and LOCATION\_NAME is the name which is displayed.
That is useful if you want to define the location as a long name or coordinates.
But you have to specify for every location a name.
```
[wttr]
command=$SCRIPT_DIR/wttr 
interval=persist
UPDATE_INTERVAL=900
UPDATE_MSG_TXT="Updating..."
LOCATION_NAME=['Berlin', 'Singapore', 'New York']
LOCATION=['52.51,13.28', 'Singapore', 'New York City, New York, USA']
ONLY_TEMP=1
START_DELAY=5
```
